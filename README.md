# README #

Version control for a talk given in the Brown HRCI in May 2017

*	Talk focuses on what Robotics research can learn from sentence processing and pragmatics literature, if the aim is to have robots that anticipate and understand instructions made by humans in an efficient manner.